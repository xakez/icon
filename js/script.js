
var startX = -1;

function fform_submit() {
   $('.fform').each(function() {
      if ($(this).val()==$(this).attr('defvalue')) {
         $(this).val('');
      }
   });
   $.ajax({
      type: 'POST',
      url: $("#fform").attr('action'),
      data: $("#fform").serialize(),
      success: function(data)
      {
         $('.fform').each(function() {
            if (!$(this).val()) {
               $(this).val($(this).attr('defvalue'));
            }
         });
         //alert(data);
         var pos = data.indexOf('success');
         if (pos > -1) {
            $('#wind1').show();
         } else {
            data = data.replace('success', '');
            $('#errortext').text(data);
            $('#wind2').show();
         }

      }
   });
}

$(window).load(function() {

   $(window).resize(function(){
      resizer();
   });


   $('.slides').addClass('sl-trans');

   setIntervalSlider();

   $('.one-menu').click(function(ev){
      ev.stopImmediatePropagation();

      var ha = $(this).hasClass("active");

      $('.one-menu').removeClass("active");

      if (!ha)
         $(this).addClass("active");

   });

   $('.left-arr, .big-left-arr, .big-left-arr2').click(function(ev){
      ev.stopImmediatePropagation();
      setIntervalSlider();
      var k = $(this).closest('.slider').find('.one-num.active');
      k = k.prev('.one-num');
      if (k.length===0){
         k = $(this).closest('.slider').find('.one-num');
         k = k.eq(k.length-1);
      }
      if (k.length>0){
         k.eq(0).trigger("click");
      }
   });

   $('.right-arr, .big-right-arr, .big-right-arr2').click(function(ev){
      ev.stopImmediatePropagation();
      setIntervalSlider();
      var k = $(this).closest('.slider').find('.one-num.active');
      k = k.next('.one-num');
      if (k.length===0){
         k = $(this).closest('.slider').find('.one-num');
         k = k.eq(0);
      }
      if (k.length>0){
         k.eq(0).trigger("click");
      }
   });

   $('.big-right-arr2').click(function(){
      var s = $(this).closest('.slider'),
         slPerson = s.find('.slides-person'),
         cm = parseInt(slPerson.attr("marginleft"),10);

      if (!cm) cm = 0;
      cm -= 100;

      if ((s.find('.one-person').length-1)*100<-cm){
         cm= 0;
      }

      s.find('.one-person').removeClass('active');
      s.find('.one-person').eq(-cm/100).addClass('active');

      slPerson.attr("marginleft", cm);
      slPerson.css({"margin-left":cm+"vw"});

   });

   $('.big-left-arr2').click(function(){
      var s = $(this).closest('.slider'),
         slPerson = s.find('.slides-person'),
         cm = parseInt(slPerson.attr("marginleft"),10);

      if (!cm) cm = 0;
      cm += 100;

      if (cm>0){
         cm=-(s.find('.one-person').length-1)*100;
      }

      s.find('.one-person').removeClass('active');
      s.find('.one-person').eq(-cm/100).addClass('active');

      slPerson.attr("marginleft", cm);
      slPerson.css({"margin-left":cm+"vw"});

   });

   $('.big-right-arr2.news-arr').click(function(){
      var s = $(this).closest('.slider'),
         slPerson = s.find('.news-slide'),
         tab = s.find('.news-slides'),
         cm = parseInt(tab.attr("marginleft"),10),
         wid = slPerson.width(),
         wall = tab.width(),
         countvis = parseInt(slPerson.attr("countvis"),10),
         pad = parseInt(slPerson.attr("pad"),10),
         widvis = countvis*(221+pad);

      if (!cm) cm=0;
      cm -= countvis*(221 + pad);

      for (;-cm+widvis>wall;){
         cm += (221 + pad);
      }


      tab.css({"margin-left":cm+"px"});
      tab.attr("marginleft", cm);

   });

   $('.big-left-arr2.news-arr').click(function(){
      var s = $(this).closest('.slider'),
         slPerson = s.find('.news-slide'),
         tab = s.find('.news-slides'),
         counttd = tab.find(".news-text").length,
         cm = parseInt(tab.attr("marginleft"),10),
         wid = slPerson.width(),
         countvis = parseInt(slPerson.attr("countvis"),10),
         pad = parseInt(slPerson.attr("pad"),10),
         widvis = countvis*(221+pad);

      if (!cm) cm=0;
      cm += countvis*(221 + pad);

      if (cm>0){
         cm = 0;
      }


      tab.css({"margin-left":cm+"px"});
      tab.attr("marginleft", cm);

   });

   $('body').click(function(){
      $('.one-menu').removeClass("active");
   });

   $('.style-input').focus(function(){
      var def = $(this).attr("defvalue"),
         val = $(this).val();
      if (def===val){
         $(this).val('');

      }
      $(this).addClass('with-val');
   });

   $('.style-input').blur(function(){
      var def = $(this).attr("defvalue"),
         val = $(this).val();
      if (def===val || !val){
         $(this).val(def);
         $(this).removeClass('with-val');

      }
   });

   $('.openwind').click(function(ev){
      ev.stopImmediatePropagation();
      fform_submit();
   });

   $('.submit-but').click(function(){
      $(this).closest("form").submit();
   });

   var fo = $('.foot'),
      way = $('.way');

   if (fo.length>0 && way.length>0){
      $('.foot').css({"margin-top":"50px"});
   }

   $(window).scroll(function(){
      var st = $('body').scrollTop()||$(window).scrollTop(),
         hw = document.documentElement.clientHeight,
         h = $('body').height(),
         foot = $('.foot');



      if (st<50){
         /*if (st<100){
            $('.head').removeClass("transforms");
            $('.social').removeClass("transforms");
            $('.center-logo').removeClass("transforms");
         }*/

         $('.head').removeClass("scrolled");
      } else {
         $('.head').addClass("transforms");
         $('.social').addClass("transforms");
         $('.center-logo').addClass("transforms");
         $('.head').addClass("scrolled");
      }
      if (foot.length>0) {
         foot = foot.offset().top;
         if (st + h > foot) {
            $('.way').addClass("stayWay");
         } else {
            $('.way').removeClass("stayWay");
         }
      }

      var pw = $('.photo-w');

      if (pw.length>0){
         var pl = pw.length;

         for (var i=0;i<pl;i++){
            var pwe = pw.eq(i),
               hpwe = pwe.height(),
               im = pwe.find('img'),
               imh = im.height(),
               ot = pwe.offset().top;

            if (ot<st+h){
               var bot = (hpwe-imh)*(h+st-ot)/h;

               pwe.find('img').css({"bottom":bot+"px"});
            }
         }
      }

   });

   $('.close-wind').click(function(){
      $(this).closest('.wind').hide();
   });



resizer();

   prSlider();
   try{
      setTimeout(function(){
         $('.slides-over').stop().animate({"opacity":1});
         $('.white-slider').css({"visibility":"visible"});
      }, 800);
   }catch(e){}
resizer();

});

var bloked = 0;
var interb ;

function setIntervalSlider(){
   if ($('.big-right-arr').length>0 && $('.slider.project').length===0 && $('.inner-full').length===0){
      try{
         clearInterval(clickInt);
      }catch(e){}
      clickInt = setInterval(function(){
         $('.big-right-arr').trigger("click");
      }, 5000);
   }
}


function phs(){
   var pss = $('.photo-s');
   for(var l=0;l<pss.length;l++){
      var ops = pss.eq(l),
         bIm = ops.css("background-image");
      bIm = bIm.split("url(")[1].split(")")[0];

      if (bIm.indexOf('"')===0){
         bIm = bIm.substr(1);
         bIm = bIm.substr(0, bIm.length-1);
      }

      (function(ops){
         var im = new Image();

         im.src = bIm;
         var int = setInterval(function(){
            var w = $('body').width(),
               wid = im.width;
            if (im.height>0 && !ops.hasClass('fix-wid')) {
               ops.height((im.height*w/wid) + "px");
               clearInterval(int);
            } else if (im.height>0){
               ops.height((im.height) + "px");
               clearInterval(int);
            }
         },100);

      })(ops);

   }
}

function resizer(){
   setTimeout(function(){
      var w = $('body').width(),
         h = $(document).height();

      var om = $('.one-menu');
      for (var i=0;i<om.length;i++){
         om.eq(i).find('.submenu').css({"padding-left":om.eq(i).offset().left+"px"});
      }

      phs();

      var f = $('.foot'),
         cont = $('.content');
      if (f.length>0){
         cont.height("auto");
         setTimeout(function(){
            var calcH = $(document).height() - 100- f.height()-1,
            way = $('.way');

            if (way.length>0){
               calcH -= way.height();
            }

            if (cont.length>0 && (cont.next('.way').length>0 || cont.next('.foot').length>0)){
               if (cont.height()<calcH){
                  way.addClass('stayWay');
                  cont.height(calcH);
               }
            }
         },10);
      }

      $('.slides-person').css({"width":$('.one-person').length*100+"vw"});
      var slider = $('.slider');
      for (var k=0;k<slider.length;k++){
         slider.eq(k).find('.slide, .sl, .big-left-arr, .big-right-arr').css({"height":slider.eq(k).height()+"px"});
         var numbs = slider.eq(k).hasClass('inner-full');
         var hn = slider.eq(k).height()-51;

         if (numbs>0){
            hn = $('body').height()-151;
         }
         slider.eq(k).find('.photo-slide, .photo-slide:not(.description-project) img, .pre-photo, .pre-photo img, .description-project, .slides-over').css({"height":hn+"px"});
         $('.not-paging .slide, .not-paging  .sl').css({"height":slider.eq(k).height()-130+"px"});
      }

      $('.one-num.active').eq(0).trigger("click");

      var ns = $('.news-slide');
      if (ns.length>0) {
         var wnews = w - 300,
            wsl = 221,
            count = parseInt(wnews/(wsl+20),10);

         if (count>6){
            count=6;
         }

         var
            pad = 20*(count-1),// wnews - wsl*count,
            padpx = pad/(count-1),
            hn = ns.height();

         wnews = wsl*(count)+pad;

         ns.find("td").css({"padding-right":padpx+"px"});
         ns.css({"margin-top":(h-150-hn)/2+"px", "width":wnews+"px"});
         ns.attr("pad",padpx);
         ns.attr("countvis",count);

      }


      $('.social > span').toggle(w>=1280);
      $('.phone').toggleClass("ph-ab",w<1100);


      var nl = $('.numbers .left-arr');
      if (nl.length>0){
         $('.numbers .way.way-top').css({"max-width": nl.offset().left-70+"px"});
      }

   }, 10);
}

function prSlider(){
   var slider = $('.slider');
   for (var i=0;i<slider.length;i++){
      var s = slider.eq(i),
         h = "",
         ps = s.find('.photo-slide'),
         img = s.find('.fl');

      if (img.length>0) {
         for (var k = 0; k < img.length; k++) {
            s.find('.slide').append($(createSlide(img.eq(k), k + 1)));
            s.find('.right-arr').before($(createNum(img.eq(k), k + 1)));
         }
      }
      if (img.length===1){
         s.find('.right-arr').hide();
         s.find('.big-right-arr').hide();
         s.find('.left-arr').hide();
         s.find('.big-left-arr').hide();
         s.find('.one-num').hide();
      }

      if (s.find('.big-left-arr2').length>0){
         s.find('.right-arr').hide();
         s.find('.big-right-arr').hide();
         s.find('.left-arr').hide();
         s.find('.big-left-arr').hide();
         s.find('.one-num').hide();
      }

      if (ps.length>0) {

         if (s.find('.description-project').length>0){
            s.find('.right-arr').before($(createNum2(s.find('.description-project'), 1)));
         }

         for (var t = 0; t < ps.length; t++) {
            s.find('.right-arr').before($(createNum2(ps.eq(t), t + 2)));
         }
      }


      addClick(s);
      setTimeout(function(){
         s.find('.one-num').eq(0).trigger("click");
      }, 10);

   }
}

function createNum(img, i){
   return "<div class='one-num' big='"+img.attr("src")+"' alt='"+img.attr("alt")+"'>"+i+"</div>";
}

function createSlide(img, i){
   var im = new Image();
   im.src= img.attr("src");
   im.src= img.attr("src");
   return "<div class='sl slide_is_"+i+"' style=\"background-image: url('"+img.attr("src")+ "'); opacity: 0;\"><div class='text-on-slide'>"+img.attr("alt")+"</div></div>";
}

function createNum2(img, i){
   return "<div class='one-num type-new' bigid='"+img.attr("id")+"' >"+i+"</div>";
}

function addClick(slider){
   slider.find('.one-num').click(function(ev){
      ev.stopImmediatePropagation();
      var s = $(this).closest(".slider");

      s.find('.one-num').removeClass("active");
      $(this).addClass("active");
      if (!$(this).hasClass("type-new")) {
         //s.find('.slide').css({"background-image": "url('" + $(this).attr('big') + "')"});
         s.find('.slide').toggleClass("transform-t");
         //s.find('.text-on-slide').html($(this).attr('alt'));
         s.find('.slide .sl').stop().animate({"opacity":0}, 1000);
         s.find('.slide_is_'+$(this).text()).stop().animate({"opacity":1}, 1000);
      } else {

         var el =  $('#'+$(this).attr('bigid')),
            l = el.offset().left,
            bw = $('body').width(),
            wid = el.width(),
            mar = l - bw/2 + wid/ 2,
            ml = parseInt(s.find('.slides').css("margin-left"),10) ;


         s.find('.slides').removeClass('sl-trans');
         var td = el.closest("td"),
            td2 = td,
            td3 = td,
            next = 0,
            prev = 0,
            table = el.closest("table");
         while(true){
            if (td2.next("td").length>0){
               td2 = td2.next("td");
               next++;
            } else {
               break;
            }
         }

         while(true){
            if (td3.prev("td").length>0){
               td3 = td3.prev("td");
               prev++;
            } else {
               break;
            }
         }

         var cp = parseInt((next-prev)/2,10);

         while (cp>0){
            var l = td2.prev("td");
            td3.before(td2);
            td3 = td3.prev("td");
            ml -= td2.width();
            td2 = l;
            cp--;
         }

         while (cp<0){
            var l = td3.next("td");
            td2.after(td3);
            td2 = td2.next("td");
            ml += td3.width();
            td3 = l;
            cp++;
         }
         var self = this;
         s.find('.slides').css({"margin-left":ml+"px"});
         setTimeout(function(){
            ml -= mar;
            if ($(self).attr('bigid')==='ps0')
               ml-=55;
            s.find('.slides').addClass('sl-trans');
            s.find('.slides').css({"margin-left":ml+"px"});
            s.find('.slides .active').removeClass('active');
            el.addClass('active');
         }, 20);


      }

   });
}





//document.onmousemove = mouseMove;

function mouseMove(event){
   event = fixEvent(event);
   var newX = event.pageX - $('.subscribe').offset().left;//-document.body.scrollTop;
   if (startX === -1)
      startX === newX;

   var mar = parseInt((newX -startX)/30,10);

   if (mar){

      $('.subscribe').css("background-position", (mar)+"px 0");

   }
}

function fixEvent(e) {
   // получить объект событие для IE
   e = e || window.event;

   // добавить pageX/pageY для IE
   if ( e.pageX == null && e.clientX != null ) {
      var html = document.documentElement;
      var body = document.body;
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
   }

   // добавить which для IE
   if (!e.which && e.button) {
      e.which = (e.button & 1) ? 1 : ( (e.button & 2) ? 3 : ( (e.button & 4) ? 2 : 0 ) );
   }

   return e;
};



